#![allow(clippy::no_effect_underscore_binding)]

use chrono::Utc;
use lazy_static::lazy_static;
use prometheus::{register_int_counter, Encoder, IntCounter, TextEncoder};
use rocket::response::content::RawText;
use rocket::{get, launch, routes};

#[get("/")]
fn index() -> RawText<String> {
    CALL_COUNTER.inc();
    let now = Utc::now();
    let iso_timestamp = now.to_rfc3339();
    RawText(iso_timestamp)
}

#[get("/metrics")]
fn metrics() -> String {
    CALL_COUNTER.inc();
    let metric_families = prometheus::gather();
    let mut buffer = vec![];
    let encoder = TextEncoder::new();
    let _ = encoder.encode(&metric_families, &mut buffer);
    String::from_utf8(buffer).unwrap_or_default()
}

#[get("/health")]
fn health() -> RawText<String> {
    CALL_COUNTER.inc();
    RawText("Ok".to_string())
}

lazy_static! {
    static ref CALL_COUNTER: IntCounter =
        register_int_counter!("call_counter", "Count calls made to this application").unwrap();
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![index, health, metrics])
}
