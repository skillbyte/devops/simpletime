#!/usr/bin/env bash

docker buildx build \
  -t kfkskillbyte/simpletime:latest \
  -f Dockerfile \
  --platform linux/arm64,linux/amd64 \
  --push \
  .
