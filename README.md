# simpletime

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Expose ISO timestamp in simple web service

This Rocket web service offers a lightweight sample workload that simply returns the current ISO timestamp. There's also
a `/metrics` endpoint that exposes a single Prometheus metric with the number of calls the service has received and a
`health` endpoint for lifeness probes.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```sh
docker pull kfkskillbyte/simpletime:latest
```

## Usage

```sh
docker run --rm -p 8000:8000 kfkskillbyte/simpletime:latest
```

You can also run from source with the following command (assuming you have the Rust toolchain installed):

```sh
cargo run
```

## Maintainers

[@kfkonrad](https://gitlab.com/kfkonrad)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the
[standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2024 Kevin F. Konrad
